# This CI/CD configuration provides a standard pipeline for
# * building a Docker image (using a buildpack if necessary),
# * storing the image in the container registry,
# * running tests from a buildpack,
# * running code quality analysis,
# * creating a review app for each topic branch,
# * and continuous deployment
#
# Test jobs may be disabled by setting environment variables:
# * code_quality: CODE_QUALITY_DISABLED
# * license_management: LICENSE_MANAGEMENT_DISABLED
# * performance: PERFORMANCE_DISABLED
# * sast: SAST_DISABLED
# * dependency_scanning: DEPENDENCY_SCANNING_DISABLED
# * container_scanning: CONTAINER_SCANNING_DISABLED
# * dast: DAST_DISABLED
# * review: REVIEW_DISABLED
# * stop_review: REVIEW_DISABLED
#
# In order to deploy, you must have a Kubernetes cluster configured either
# via a project integration, or via group/project variables.
# KUBE_INGRESS_BASE_DOMAIN must also be set on the cluster settings,
# as a variable at the group or project level, or manually added below.
#

image: alpine:latest

variables:
  # KUBE_INGRESS_BASE_DOMAIN is the application deployment domain and should be set as a variable at the group or project level.
  # KUBE_INGRESS_BASE_DOMAIN: domain.example.com

  KUBERNETES_VERSION: 1.28.8
  HELM_VERSION: 3.14.4

  DOCKER_DRIVER: overlay2

  ROLLOUT_RESOURCE_TYPE: deployment

  AUTO_BUILD_IMAGE_VERSION: 'v2.0.0'

stages:
  - build
  - staging
  - production

before_script:
  - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

build:
  stage: build
  image: '${CI_TEMPLATE_REGISTRY_HOST}/gitlab-org/cluster-integration/auto-build-image:${AUTO_BUILD_IMAGE_VERSION}'
  variables:
    DOCKER_TLS_CERTDIR: ''
  services:
    - name: 'docker:20.10.12-dind'
      command: ['--tls=false', '--host=tcp://0.0.0.0:2375']
  script:
    - if [ ! -f Dockerfile ]; then wget -q https://gitlab.com/GoldenHippoMedia/ci-library/wordpress/raw/v5.8.2-phpv7.4-sast/Dockerfile; fi
    - echo -e ".git/\n.gitignore\n.gitlab-ci.yml\nREADME.md" > .dockerignore
    - /build/build.sh
  only:
    - branches
    - tags

staging:
  stage: staging
  script:
    - check_kube_domain
    - install_dependencies
    - download_chart
    - use_kube_context
    - ensure_namespace
    - create_secret
    - deploy
    - persist_environment_url
  environment:
    name: staging
    url: http://$CI_PROJECT_PATH_SLUG-staging.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt]
  only:
    refs:
      - staging

.production: &production_template
  stage: production
  script:
    - check_kube_domain
    - install_dependencies
    - download_chart
    - use_kube_context
    - ensure_namespace
    - create_secret
    - deploy
    - persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt]

production:
  <<: *production_template
  only:
    refs:
      - master
      - main

include:
  - template: Jobs/SAST.gitlab-ci.yml
  
# ---------------------------------------------------------------------------

.auto_devops: &auto_devops |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x
  if [[ -z "$CI_COMMIT_TAG" ]]; then
    export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    export CI_APPLICATION_TAG=$CI_COMMIT_SHA
  else
    export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE
    export CI_APPLICATION_TAG=$CI_COMMIT_TAG
  fi

  function get_replicas() {
    track="${1:-stable}"
    percentage="${2:-100}"

    env_track=$( echo $track | tr -s  '[:lower:]'  '[:upper:]' )
    env_slug=$( echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s  '[:lower:]'  '[:upper:]' )

    if [[ "$track" == "stable" ]] || [[ "$track" == "rollout" ]]; then
      # for stable track get number of replicas from `PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        new_replicas=$REPLICAS
      fi
    else
      # for all tracks get number of replicas from `CANARY_PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_track}_${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        eval new_replicas=\${env_track}_REPLICAS
      fi
    fi

    replicas="${new_replicas:-1}"
    replicas="$(($replicas * $percentage / 100))"

    # always return at least one replicas
    if [[ $replicas -gt 0 ]]; then
      echo "$replicas"
    else
      echo 1
    fi
  }

  function create_application_secret() {
    track="${1-stable}"
    export APPLICATION_SECRET_NAME=$(application_secret_name "$track")

    env | sed -n "s/^K8S_SECRET_\(.*\)$/\1/p" > k8s_prefixed_variables

    kubectl create secret \
      -n "$KUBE_NAMESPACE" generic "$APPLICATION_SECRET_NAME" \
      --from-env-file k8s_prefixed_variables -o yaml --dry-run=client |
      kubectl replace -n "$KUBE_NAMESPACE" --force -f -

    export APPLICATION_SECRET_CHECKSUM=$(cat k8s_prefixed_variables | sha256sum | cut -d ' ' -f 1)

    rm k8s_prefixed_variables
  }

  function deploy_name() {
    name="$CI_ENVIRONMENT_SLUG"
    track="${1-stable}"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    echo $name
  }

  function application_secret_name() {
    track="${1-stable}"
    name=$(deploy_name "$track")

    echo "${name}-secret"
  }

  function deploy() {
    track="${1-stable}"
    percentage="${2:-100}"
    name=$(deploy_name "$track")

    replicas="1"
    service_enabled="true"
    postgres_enabled="$POSTGRES_ENABLED"

    # if track is different than stable,
    # re-use all attached resources
    if [[ "$track" != "stable" ]]; then
      service_enabled="false"
      postgres_enabled="false"
    fi

    replicas=$(get_replicas "$track" "$percentage")

    if [[ "$CI_PROJECT_VISIBILITY" != "public" ]]; then
      secret_name='gitlab-registry'
    else
      secret_name=''
    fi

    create_application_secret "$track"

    env_slug=$(echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s '[:lower:]' '[:upper:]')
    eval env_ADDITIONAL_HOSTS=\$${env_slug}_ADDITIONAL_HOSTS
    if [ -n "$env_ADDITIONAL_HOSTS" ]; then
      additional_hosts="{$env_ADDITIONAL_HOSTS}"
    elif [ -n "$ADDITIONAL_HOSTS" ]; then
      additional_hosts="{$ADDITIONAL_HOSTS}"
    fi

    echo "Deploying new release..."
    helm upgrade --install \
      --wait \
      --set service.enabled="$service_enabled" \
      --set releaseOverride="$CI_ENVIRONMENT_SLUG" \
      --set image.repository="$CI_APPLICATION_REPOSITORY" \
      --set image.tag="$CI_APPLICATION_TAG" \
      --set image.pullPolicy=IfNotPresent \
      --set image.secrets[0].name="$secret_name" \
      --set application.track="$track" \
      --set application.secretName="$APPLICATION_SECRET_NAME" \
      --set application.secretChecksum="$APPLICATION_SECRET_CHECKSUM" \
      --set service.commonName="le.$KUBE_INGRESS_BASE_DOMAIN" \
      --set service.url="$CI_ENVIRONMENT_URL" \
      --set service.additionalHosts="$additional_hosts" \
      --set replicaCount="$replicas" \
      --namespace="$KUBE_NAMESPACE" \
      "$name" \
      chart/

    kubectl rollout status -n "$KUBE_NAMESPACE" -w "$ROLLOUT_RESOURCE_TYPE/$name"
  }

  function install_dependencies() {
    apk add -U openssl curl tar gzip bash ca-certificates git
    curl -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    curl -L -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.32-r0/glibc-2.32-r0.apk
    apk add --force-overwrite glibc-2.32-r0.apk
    rm glibc-2.32-r0.apk

    wget https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
    tar -xf helm-v${HELM_VERSION}-linux-amd64.tar.gz
    mv linux-amd64/helm /usr/bin/helm && rm -rf linux-amd64/
    chmod 600 ../${CI_PROJECT_NAME}.tmp/KUBECONFIG
    helm version

    curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client
  }

  function download_chart() {
    if [[ ! -d chart ]]; then
      auto_chart=${AUTO_DEVOPS_CHART:-gitlab/auto-deploy-app}
      auto_chart_name=$(basename $auto_chart)
      auto_chart_name=${auto_chart_name%.tgz}
      auto_chart_name=${auto_chart_name%.tar.gz}
    else
      auto_chart="chart"
      auto_chart_name="chart"
    fi

    echo ${AUTO_DEVOPS_CHART_REPOSITORY}
    
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm repo add gitlab ${AUTO_DEVOPS_CHART_REPOSITORY:-https://charts.gitlab.io}

    if [[ ! -d "$auto_chart" ]]; then
      helm fetch ${auto_chart} --untar
    fi
    if [ "$auto_chart_name" != "chart" ]; then
      mv ${auto_chart_name} chart
    fi

    helm dependency update chart/
    helm dependency build chart/
  }

  function use_kube_context() {
    if [[ -z "$KUBE_CONTEXT" ]]; then
      echo "KUBE_CONTEXT not defined. The default context (if present) will be used"
      return
    fi
    echo "Using context '$KUBE_CONTEXT'"
    kubectl config use-context "$KUBE_CONTEXT"
  }

  function ensure_namespace() {
    kubectl describe namespace "$KUBE_NAMESPACE" || kubectl create namespace "$KUBE_NAMESPACE"
  }

  # Function to ensure backwards compatibility with AUTO_DEVOPS_DOMAIN
  function ensure_kube_ingress_base_domain() {
    if [ -z ${KUBE_INGRESS_BASE_DOMAIN+x} ] && [ -n "$AUTO_DEVOPS_DOMAIN" ] ; then
      export KUBE_INGRESS_BASE_DOMAIN=$AUTO_DEVOPS_DOMAIN
    fi
  }

  function check_kube_domain() {
    ensure_kube_ingress_base_domain

    if [ -z ${KUBE_INGRESS_BASE_DOMAIN+x} ]; then
      echo "In order to deploy or use Review Apps,"
      echo "AUTO_DEVOPS_DOMAIN or KUBE_INGRESS_BASE_DOMAIN variables must be set"
      echo "From 11.8, you can set KUBE_INGRESS_BASE_DOMAIN in cluster settings"
      echo "or by defining a variable at group or project level."
      echo "You can also manually add it in .gitlab-ci.yml"
      echo "AUTO_DEVOPS_DOMAIN support will be dropped on 12.0"
      false
    else
      true
    fi
  }

  function create_secret() {
    echo "Create secret..."
    if [[ "$CI_PROJECT_VISIBILITY" == "public" ]]; then
      return
    fi

    kubectl create secret -n "$KUBE_NAMESPACE" \
      docker-registry gitlab-registry \
      --docker-server="$CI_REGISTRY" \
      --docker-username="${CI_DEPLOY_USER:-$CI_REGISTRY_USER}" \
      --docker-password="${CI_DEPLOY_PASSWORD:-$CI_REGISTRY_PASSWORD}" \
      --docker-email="$GITLAB_USER_EMAIL" \
      -o yaml --dry-run=client | kubectl replace -n "$KUBE_NAMESPACE" --force -f -
  }

  function persist_environment_url() {
      echo $CI_ENVIRONMENT_URL > environment_url.txt
  }

before_script:
  - *auto_devops
